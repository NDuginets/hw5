function Zoo (name) {
    this.name = name;
    this.animalsCount = 0;
    this.zones = {
        mammals: [],
        birds: [],
        fishes: [],
        reptile: [],
        others: []
   };

    this.addAnimal = function (animalObj) {
     if (animalObj.zone ) {
        if (animalObj.zone == "mammals") {
            this.zones.mammals.push(animalObj);
        } else if (animalObj.zone == "birds") {
            this.zones.birds.push(animalObj);
        } else if (animalObj.zone == "fishes") {
            this.zones.fishes.push(animalObj);
        } else if (animalObj.zone == "reptile") {
            this.zones.reptile.push(animalObj);
        } 
    }   
        else {
            this.zones.reptile.push(animalObj);
        }
    };

    this.removeAnimal = function (animalName) {
        for (key in this.zones) {
            
            for (var i=0; i<this.zones[key].length+1; i++) {
                if (this.zones[key][i]) {  // тут была ошибка, мол хочет достучаться до .name того, чего еще нету.
                    if (this.zones[key][i].name == animalName) {
                    this.zones[key].splice(i,1);
                    
                    console.log('++');
                    break;

                    }
                }   
            
            }
        }
    }

    this.getAnimal = function (type, value) {
        if (type == 'type') {
         let rezArr = [];
            for (key in this.zones) {
                
                for (var i=0; i<this.zones[key].length+1; i++) {
                    if (this.zones[key][i]) {  
                        if (this.zones[key][i].type == value) {
                            rezArr.push(this.zones[key][i])

                        }
                    }   
                
                }
            }
         console.log(rezArr);
        }

        if (type == 'name') {
            for (key in this.zones) {
                
                for (var i=0; i<this.zones[key].length+1; i++) {
                    if (this.zones[key][i]) {  
                        if (this.zones[key][i].name == value) {
                        console.log(this.zones[key][i]);
                        break;
    
                        }
                    }   
                
                }
            }
        }
        
    }

    this.countAnimals = function () {
        let count = 0;
        for (key in this.zones) {
           count += this.zones[key].length;
        }   
    console.log(`Всего в зоопарке ${count} животных`);
    this.animalsCount = count;
        
    }
};


class Animal {
    constructor (name, phrase, foodType) {
        this.name = name;
        this.phrase = phrase;
        this.foodType = foodType;
    }

    eatSomething () {
        console.log(`Это животное кушает ${this.foodType}`)
    }
}


class Mammals extends  Animal {
    constructor (name, phrase, type, speed, foodType) {
        super(name, phrase, foodType);
        this.zone = "mammals";
        this.type = type; //Эту строку и след. строку я бы добавил в класс Анимал, ибо они повторяються у всех других классах 
        this.speed = speed; //Но я просто делал как было в задании, ничего лишнего не выдумывал.
  }
    
    run () {
        console.log(`${this.type} ${this.name} run with speed ${this.speed} km/h`)
    }

}

class Birds extends  Animal {
    constructor (name, phrase, type, speed, foodType) {
        super(name, phrase, foodType);
        this.zone = "birds";
        this.type = type;
        this.speed = speed;
  }
    
    fly () {
        console.log(`${this.type} ${this.name} fly with speed ${this.speed} km/h`)
    }

}

class Fishes extends  Animal {
    constructor (name, phrase, type, speed, foodType) {
        super(name, phrase, foodType);
        this.zone = "fishes";
        this.type = type;
        this.speed = speed;
  }
    
    swimm () {
        console.log(`${this.type} ${this.name} swimms with speed ${this.speed} km/h`)
    }

}


class Reptile extends  Animal {
    constructor (name, phrase, type, speed, foodType) {
        super(name, phrase, foodType);
        this.zone = "reptile";
        this.type = type;
        this.speed = speed;
  }
    
    run () {
        console.log(`${this.type} ${this.name} run with speed ${this.speed} km/h`)
    }

}